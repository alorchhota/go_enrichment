### What is this repository for? ###

* GO enrichment analysis
* Version: 1.0

### How do I get set up? ###

* Clone the repo.
* Enter the repo directory in shell script and run the following script.

```
#!shell script
Rscript go_enrichment.R -bg BGFILE -test TESTFILE ontology ONTOLOGY -db DB -size SIZE -bf BF -o OUTFILE
```

* When you run the script, it install all required packages automatically.

## Script options ##
```
#!help
-bg: background gene file (one gene per line). [default: data//background.genes.example.txt]
-test: test gene file (one gene per line) [default: data//test.genes.example.txt]
-ontology: BP/MF/CC (biological process/ molecular function / cellular component). [default: BP]
-db: GO annotation databse to use (org.xx.xx.db). [default: org.Hs.eg.db]
-size: min genes annotated per GO term. [default: 5]
-bf: bonferroni correction [default: TRUE]
-o: output file. [default: results//go_enrichment.out]
```
### Contact ###

* ashis@jhu.edu

